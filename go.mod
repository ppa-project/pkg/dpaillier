module ci.tno.nl/gitlab/ppa-project/pkg/dpaillier

go 1.15

require (
	ci.tno.nl/gitlab/ppa-project/pkg/paillier v1.0.1
	ci.tno.nl/gitlab/ppa-project/pkg/secret v0.5.0
	github.com/stretchr/testify v1.7.0
)

replace ci.tno.nl/gitlab/ppa-project/pkg/paillier => ci.tno.nl/gitlab/ppa-project/pkg/paillier.git v1.0.1

replace ci.tno.nl/gitlab/ppa-project/pkg/secret => ci.tno.nl/gitlab/ppa-project/pkg/secret.git v0.5.0
